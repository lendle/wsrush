
<%@page import="ilab.apprush.core.security.User"%>
<%@page import="ilab.apprush.core.security.keystore.Key"%>
<%@page import="ilab.wsrush.Constants"%>
<%@page import="ilab.apprush.core.security.keystore.Keystore"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%
    String id = request.getParameter("id");
    String pass = request.getParameter("password");
    Keystore keystore=(Keystore)WebApplicationContextUtils.getWebApplicationContext(application).getBean("keystore");
    Key key=(Key)keystore.getKey(id, pass);
    User user=keystore.getUserObject(key.getKeyString());
    if (key!=null && user.getGroup().equals("admin")) {
        session.setAttribute(Constants.LOGIN_USER, id);
        
        if (session.getAttribute("requestURI") == null) {
            response.sendRedirect("/WSRush/admin/index.jsp");
        } else {
            String uri = "" + session.getAttribute("requestURI");
            if (uri.equals("/WSRush")) {
                uri = uri + "/index.jsp";
            }
            response.sendRedirect(uri);
        }
    } else {
        out.println("<h1>Login Fail!</h1>");
    }
%>