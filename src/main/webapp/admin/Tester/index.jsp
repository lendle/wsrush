<%-- 
    Document   : index
    Created on : May 23, 2016, 3:21:45 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>WSRush Tester</title>
        <script type="text/javascript" src="../../js/config.js.jsp"></script>
        <script src="../../js/jquery-1.11.2.min.js" type="text/javascript"></script>
        <script src="../../js/jquery.blockUI.js" type="text/javascript"></script>
        <script src="../../js/jquery.cookie.js" type="text/javascript"></script>
        <script src="../../js/application.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/redmond/jquery-ui.css"/>
        <script  src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script type="text/javascript">
            function addRow(self) {
                var tr = document.createElement("tr");
                $(tr).attr("class", "param");
                $(tr).html("<td style='width: 40%'>參數：<input  class='paramName' type=\"text\"/></td><td style='width: 40%'>內容：<input type=\"text\" class='paramValue'/></td><td><input style='width: 20%' type='button' value='-' onclick='removeRow(this);'/></td>");
                $(tr).insertBefore($(self.parentNode.parentNode));
            }

            function removeRow(self) {
                var tr = self.parentNode.parentNode;
                tr.parentNode.removeChild(tr);
            }

            function execute() {
                var params = {};
                $(".param").each(function () {
                    var name = $(this).find(".paramName").val();
                    var value = $(this).find(".paramValue").val();
                    if (name && name != "") {
                        if (value.trim().indexOf("[") == 0) {
                            value = JSON.stringify(JSON.parse("" + value));
                        } else if (value.trim().indexOf("{") == 0) {
                            value = JSON.stringify(JSON.parse("" + value));
                        }
                        params[name] = value;
                    }
                });
                application.callAPI($("#namespace").val(), $("#localName").val(), params,
                    function (data) {
                        $("#result").val(JSON.stringify(data, null, '\t'));
                    }
                );
            }
        </script>
    </head>
    <body>
        <div style="position: absolute; top: 0px; height: 80px">
            Namespace: <input type="text" id="namespace"/><br/>
            Local Name: <input type="text" id="localName"/><br/>
        </div>
        <div style="position: absolute; top: 80px; bottom: 50px; width: 95%">
            <div style="position: absolute; top: 0px; width: 50%; height: 100%; border-style: solid; border-width: 1px; flow: left">
                <div style="position: relative; overflow: auto; width: 100%; height: 100%;">
                    <div style="position: relative;  width: 100%; ">
                        <table style="width: 100%;" id="params" border="1">
                            <tr class="param">
                                <td style="width: 40%">參數：<input type="text" class="paramName"/></td>
                                <td style="width: 40%">內容：<input type="text" class="paramValue"/></td>
                                <td style="width: 20%"></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <input type="button" onclick="addRow(this);" value="+"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div style="position: absolute; top: 0px; width: 50%; height: 100%; left: 50%; right: 0%; border-style: solid; border-width: 1px; flow: right">
                <textarea style="position: relative; width: 99%; height: 99%" id="result"></textarea>
            </div>
            <!--table border="1" style="width: 100%; height: 100%">
                <tr>
                    <td style="width: 50%; vertical-align: top">
                        
                    </td>
                    <td style="width: 50%">
                        <textarea style="position: absolute; top: 10px; height: 90%; width: 45%"></textarea>
                    </td>
                </tr>
            </table-->
        </div>
        <div style="position: absolute; bottom: 0px; height: 50px; width: 95%">
            <input type="button" value="GO!" style="width: 100%" onclick="execute();"/>
        </div>
    </body>
</html>
