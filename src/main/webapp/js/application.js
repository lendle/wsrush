/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function ApplicationClass() {
    var self=this;
    
    this.init = function () {

    }

    this._callAPI = function (namespace, localName, parameters, successCallback, failCallback, headers) {
        $.ajax("http://" + SERVER_CONTEXT_ROOT + "/ws/dispatcher?namespace=" + namespace + "&localName=" + localName, {
            "method": "POST",
            "data": parameters,
            "headers": headers,
            "success": function (data) {
                console.log(data);
                if (data.sessionId) {
                    $.cookie("sessionId", data.sessionId);
                }
                if(data.error && data.errorCode=="authentication"){
                    $.removeCookie("sessionId");
                    self.callAPI(namespace, localName, parameters, successCallback, failCallback, true);
                    return;
                }
                if (successCallback) {
                    successCallback(data.value);
                }
            },
            "error": function (jqXHR, textStatus, errorThrown) {
                if (failCallback) {
                    failCallback(jqXHR, textStatus, errorThrown);
                }
            }
        });
    }

    this.callAPI = function (namespace, localName, parameters, successCallback, failCallback, requestLogin) {
        var headers = {};
        if ($.cookie("sessionId")) {
            headers.sessionId = $.cookie("sessionId");
            self._callAPI(namespace, localName, parameters, successCallback, failCallback, headers);
        } else {
            if(requestLogin){
                if (document.body) {
                    $("#__apprush_login__").dialog({
                        modal: true,
                        buttons: [
                            {
                                text: "OK",
                                click: function () {
                                    headers.__apprush_user__ = $("#__apprush_user__").val();
                                    headers.__apprush_password__ = $("#__apprush_password__").val();
                                    $(this).dialog("close");
                                    self._callAPI(namespace, localName, parameters, successCallback, failCallback, headers);
                                }
                            }
                        ]
                    });
                }
            }else{
                self._callAPI(namespace, localName, parameters, successCallback, failCallback, headers);
            }

        }
    }

    this.close = function () {

    }
}

var application = null;

$(document).ready(function () {
    application = new ApplicationClass();
    application.init();

    if ($.blockUI) {
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    }

    if (document.body) {
        $(document.body).append("<div id='__apprush_login__' style='display:none'>ID: <input id='__apprush_user__' /><br/>PASS:<input id='__apprush_password__'  type='password'/></div>");
    }
});

$(window).unload(function () {
    if (application) {
        application.close();
    }
});
