<%@page import="java.net.Inet4Address"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.util.Collections"%>
<%@page import="java.net.NetworkInterface"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Enumeration"%>
<%
    String ip=null;
    Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
    for (NetworkInterface netint : Collections.list(nets)) {
        Enumeration en = netint.getInetAddresses();
        while (en.hasMoreElements()) {
            InetAddress address = (InetAddress) en.nextElement();
            if (address instanceof Inet4Address && (!address.isAnyLocalAddress()) && !(address.isLoopbackAddress())) {
                ip=address.toString().substring(1);
            }
        }
    }
%>
var SERVER_CONTEXT_ROOT="<%=ip%>:8080/WSRush";

