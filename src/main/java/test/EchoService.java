/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import ilab.apprush.core.ExecutionResult;
import ilab.apprush.core.ServiceRequestParameter;
import ilab.apprush.core.services.AbstractService;

/**
 *
 * @author lendle
 */
public class EchoService extends AbstractService{

    @Override
    public ExecutionResult execute(ServiceRequestParameter parameter) throws Exception {
        return this.createExecutionResult(null, true, parameter.getParameter("value"));
    }

    @Override
    public boolean isAuthenticatedSessionRequired() {
        return true;
    }
    
}
